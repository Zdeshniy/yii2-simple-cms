/**
 * Created by arseniy on 08/10/14.
 */
$(document).ready(function(){
  var lock_load = true;
  var count_page = 0;
  $(document).on('scroll', function(){
    var sc = document.documentElement.scrollHeight - $('body').scrollTop()-$('body').height();
    if(sc <400 && lock_load){
      lock_load = false;
      count_page ++;
      $.ajax({
        url: '',
        data: {count_page: count_page},
        type: "POST",
        success: function(r){
          $('#LastPost').append(r);
          lock_load = true;
        }
      });
    }
  });
});