<?php
namespace frontend\modules\site;

use yii\web\AssetBundle;

/**
 * Пакет FancyBox плагина
 */
class FancyBoxAsset extends AssetBundle
{
	public $sourcePath = '@frontend/modules/site/assets/vendor/fancybox2';
	public $css = [
		'source/etoservis.jquery.fancybox.css'
	];
	public $js = [
	    'lib/jquery.mousewheel-3.0.6.pack.js',
	    'source/jquery.fancybox.pack.js'
	];
	public $depends = [
	    'frontend\modules\site\AppAsset',
		'yii\web\JqueryAsset'
	];
}