<?php
namespace frontend\modules\site\covers;

use yii\web\AssetBundle;

class ActiveFormAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/site/assets';
    public $js = [
        'js/ActiveForm.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}