<?php
/**
 * Основной шаблон приложения
 * @var $this \yii\base\View
 * @var $content string
 * @var $params array основные параметры View
 */


use frontend\modules\blogs\widgets\news\News;
use frontend\modules\blogs\widgets\blocks4\Blocks4;
use frontend\modules\blogs\widgets\lastpost\LastPost;
use frontend\modules\blogs\widgets\archive\Archive;
use frontend\modules\blogs\widgets\subscription\Subscription;
use frontend\modules\site\BlogsAsset;

BlogsAsset::register($this);
?>

<?php $this->beginContent('@frontend/modules/site/views/layouts/layout.php'); ?>
    <!-- Content -->
    <div id="content">
        <!-- Container -->
        <div class="container-fluid">

            <?= Blocks4::widget([
              'secondaryTitle' => 'Интересное: '
            ]); ?>

          <?= $content; ?>

        </div>
        <!--/ Container -->
    </div>
    <!--/ Content -->
<?php $this->endContent(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/instagram/spectragram.min.js"></script>
<script type="text/javascript" src="/js/instagram/settings.js"></script>
