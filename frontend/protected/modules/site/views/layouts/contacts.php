<?php

/**
 * Основной шаблон приложения
 * @var $this \yii\base\View
 * @var $content string
 * @var $params array основные параметры View
 */

use yii\helpers\Html;
use yii\captcha\Captcha;
use frontend\modules\site\covers\ActiveForm;
use frontend\modules\site\models\ContactForm;

$model = new ContactForm();
?>
<?php $this->beginContent('@frontend/modules/site/views/layouts/layout.php'); ?>
    <!-- Content -->
    <section id="content">
    	<!-- Container -->
    	<div class="container-fluid">
    		<?= $content; ?>

	    	<!-- Feedback -->
	    	<div class="feedback">
				<h3>Письмо</h3>
				<h2>Приглашаем вас на диалог</h2>
				<p>Пункты, помеченные *, обязательны к заполнению.</p>

				<?php if (Yii::$app->session->hasFlash('feedback')) { ?>
					<p><?= Yii::$app->session->getFlash('feedback') ?></p>
				<?php } ?>

				<?php $form = ActiveForm::begin([
					'action' => ['/site/default/mail'],
					'fieldConfig' => [
						'template' => "{label}\n{input}"
					]
				]); ?>
					<p class="success-msg">Письмо отправлено!</p>
					<div class="row">
						<?= $form->field($model, 'surname') ?>
						<?= $form->field($model, 'name') ?>
					</div>
					<div class="row">
						<?= $form->field($model, 'email') ?>
						<?= $form->field($model, 'phone') ?>
					</div>
					<div class="row row-full">
						<?= $form->field($model, 'body')->textarea() ?>
					</div>
					<div class="row row-last">
						<a href="/privacy/">Ознакомится с условиями обработки персональных данных.</a>
						<?= $form->field($model, 'verifyCode', ['template' => '{input}'])->widget(Captcha::className(), [
							'captchaAction' => '/site/default/captcha'
						]); ?>
						<?= Html::submitButton('Отправить') ?>
					</div>
				<?php ActiveForm::end(); ?>
			</div>
			<!--/ Feedback -->

		</div>
		<!--/ Container -->
	</section>
	<!--/ Content -->
<?php $this->endContent(); ?>