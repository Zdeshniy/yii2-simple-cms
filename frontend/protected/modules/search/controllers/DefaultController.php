<?php

namespace frontend\modules\search\controllers;

use common\modules\blogs\models\Post;
use common\modules\galleries\models\Image;
use common\modules\materials\models\Material;
use common\modules\pages\models\Page;
use common\modules\tags\helpers\TagsHelper;
use common\modules\tags\models\Tag;
use frontend\modules\site\components\Controller;
use Yii;

/**
 * Основной контроллер модуля [[Pages]]
 */
class DefaultController extends Controller
{
    /**
     * Страница поиска по тэгам
     * @param string $tag
     * @return mixed
     */
    public function actionIndex($tag)
    {
        $_tag = Tag::find()->where(['name' => $tag])->with(['models','seo'])->asArray()->one();
        $galleries = $posts = $pages = $materials = [];

        if ($_tag) {
            $models = [];
            foreach ($_tag['models'] as $tag) {
                $models[$tag['model_class_id']][] = $tag['model_id'];
            }

            $galleryClass = TagsHelper::crcClassName(Image::className());
            $postClass = TagsHelper::crcClassName(Post::className());
            $pageClass = TagsHelper::crcClassName(Page::className());
            $materialClass = TagsHelper::crcClassName(Material::className());

            if (isset($models[$galleryClass])) {
                $galleries = Image::find()->where(['id' => $models[$galleryClass]])->orderBy(['create_time' => SORT_DESC])->all();
            }
            if (isset($models[$postClass])) {
                $posts = Post::find()->where(['id' => $models[$postClass]])->orderBy(['create_time' => SORT_DESC])->asArray()->all();
            }
            if (isset($models[$pageClass])) {
                $pages = Page::find()->where(['id' => $models[$pageClass]])->orderBy(['create_time' => SORT_DESC])->all();
            }
            if (isset($models[$materialClass])) {
                $materials = Material::find()->where(['id' => $models[$materialClass]])->orderBy(['create_time' => SORT_DESC])->all();
            }
        }

        return $this->render('index', [
            'model' => $_tag,
            'galleries' => $galleries,
            'posts' => $posts,
            'pages' => $pages,
            'materials' => $materials
        ]);
    }
}
