<?php
namespace frontend\modules\slider\widgets\slider;

use yii\web\AssetBundle;

/**
 * основной пкет приложения
 */
class SliderAsset extends AssetBundle
{
	public $sourcePath = '@frontend/modules/slider/widgets/slider/assets';
	public $js = [
		'js/slider.js'
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}