<?php
/**
 * Страница всех галлерей
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $models
 */

use yii\helpers\Html;
use yii\helpers\Json;
use frontend\modules\site\FancyBoxAsset;

$this->title = 'Галерея работ';
$this->params['pageClass'] = 'gallery';
?>
<?php if ($models) :
    $fancyboxModels = []; ?>
    <?php foreach ($models as $model) : ?>
        <h1 class="container-fluid"><?= $model['title'] ?></h1>
        <?php if ($model->galleries) : ?>
            <?php foreach ($model->galleries as $gallery) : ?>
                <div class="gallery">
                    <h2 class="container-fluid"><?= $gallery->title ?></h2>
                    <?php if ($gallery->images) : ?>
                        <?php foreach ($gallery->images as $image) :
                            $fancyboxModels[$image->url] = [
                                'title' => $image['title'],
                                'content' => $image['description'],
                                'tags' => ''
                            ]; ?>
                            <?php if ($image->tags) {
                                foreach ($image->tags as $key => $tag) {
                                    if ($key !== 0) {
                                        $fancyboxModels[$image->url]['tags'] .= ', ';
                                    }
                                    $fancyboxModels[$image->url]['tags'] .= Html::a('#' . $tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
                                }
                            } ?>
                            <a href="<?= $image->url ?>" class="fancybox" rel="group" target="_blank" data-hash="<?= $image['id'] ?>">
                                <img src="<?= $image->thumb ?>" alt="<?= $image['title'] ?>" title="<?= $image['title'] ?>" />
                            </a>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php endif;
    endforeach;

    FancyBoxAsset::register($this);
    $fancyboxModels = Json::encode($fancyboxModels);
    $this->registerJs('var fancyboxModels = ' . $fancyboxModels . ';
        jQuery(".fancybox").fancybox({
            helpers: {
                title: {
                    type: "inside"
                }
            },
            padding: 0,
            margin: 0,
            afterLoad: function(current, previous) {
                var content = "<div class=\"fancybox-info\"><h1>" + fancyboxModels[this.href]["title"] + "</h1>";
                if (fancyboxModels[this.href]["content"]) {
                    content += fancyboxModels[this.href]["content"];
                }
                if (fancyboxModels[this.href]["tags"]) {
                    content += "<div class=\"tags\"><p>Тэги: " + fancyboxModels[this.href]["tags"] + "</p></div>";
                }
                content += "</div>";

                $.extend(this, {
                    aspectRatio: false,
                    type: "html",
                    width: "100%",
                    height: "100%",
                    content: "<div class=\"fancybox-image\" style=\"background-image:url(" + this.href + ");\">" + content + "</div>"
                });
            },
            beforeShow: function() {
                window.location.hash = this.element.data("hash");
            },
            beforeClose: function() {
                window.location.hash = "";
            }
        });
        var hash = window.location.hash.replace("#", "");
        if (hash) {
            jQuery(".gallery a[data-hash=\"" + hash + "\"]").trigger("click");
        }');
endif;