<?php
namespace frontend\modules\pages\controllers;

use common\modules\pages\models\Page;
use frontend\modules\site\components\Controller;
use yii\web\HttpException;
use Yii;

/**
 * Основной контроллер модуля [[Pages]]
 */
class DefaultController extends Controller
{
    /**
     * Страница одной модель
     * @param string $alias
     * @return mixed
     */
    public function actionView($parent = null, $alias)
    {
        $model = $parent !== null ? $this->findModel($alias, $parent) : $this->findModel($alias);

        if ($model->seo['layout']) {
            $this->layout = $model->seo['layout'];
        }

        return $this->render('view', ['model' => $model]);
    }

    /**
     * Поиск модели по [[alias]] полю
     * Если модель не найдена, будет вызвана 404 ошибка
     * @param string $alias
     * @return Page Найденая модель
     * @throws HttpException Если модель не найдена
     */
    protected function findModel($alias, $parent = null)
    {
        if ($parent === null) {
            $model = Page::find()
                ->where(['alias' => $alias])
                ->parents()
                ->one();
        } else {
            $model = Page::find()
                ->where([Page::tableName() . '.alias' => $alias])
                ->innerJoinWith('parent')
                ->andWhere(['parent.alias' => $parent])
                ->one();
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
}
