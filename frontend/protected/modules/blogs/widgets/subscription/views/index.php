<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;
use frontend\modules\site\covers\ActiveForm;
?>
<div id="subscription-widget" class="widget">
    <?php if ($this->context->title !== null) { ?>
        <h2><?= $this->context->title ?></h2>
    <?php } ?>
    <?php $form = ActiveForm::begin([
        'action' => ['/blogs/default/subscription']
    ]); ?>
        <?= $form->field($model, 'email', ['template' => "{input}"]) ?>
        <?= Html::submitButton('Подписаться') ?>
        <?= Html::error($model, 'email', ['class' => 'help-block']) ?>
        <p class="success-msg">Ваша почта добавлена!</p>
        <p>Доставка информации по электронной почте.</p>
    <?php ActiveForm::end(); ?>
</div>