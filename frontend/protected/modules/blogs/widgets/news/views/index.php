<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html; ?>
<section id="news-widget" class="widget">
    <?php if ($this->context->secondaryTitle !== null) { ?>
        <h2><?= $this->context->secondaryTitle ?></h2>
    <?php } ?>
    <?php if ($this->context->title !== null) { ?>
        <h1><?= $this->context->title ?></h1>
    <?php } ?>

    <?php if ($models) : ?>
        <?php foreach ($models as $model) : ?>
            <!-- Item -->
            <article>
				<?php $viewPrefix = $model->categoryIds[0] == 2 ? 'faq-' : ''; ?>
                <h3><?= Html::a($model['title'], ['/blogs/default/' . $viewPrefix . 'view', 'alias' => $model['alias']]) ?></h3>
                <p>
                    <time pubdate="<?= $model->createTime ?>"><?= $model->createTime ?></time>
                    <?php if ($model->tags) :
                        $tags = '';
                        foreach ($model->tags as $key => $tag) :
                            if ($key !== 0) {
                                $tags .= ', ';
                            }
                            $tags .= Html::a('#' . $tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
                        endforeach; ?>
                        <span>/ <?= $tags ?></span>
                    <?php endif; ?>
                </p>
                <div>
                    <?php if ($model->preview) { ?>
                        <?= Html::a(Html::img($model->preview, ['alt' => $model['title'], 'title' => $model['title']]), ['/blogs/default/' . $viewPrefix . 'view', 'alias' => $model['alias']]) ?>
                    <?php } ?>
                    <p><?= $model['snippet'] ?></p>
                </div>
            </article>
            <!--/ Item -->
        <?php endforeach; ?>
    <?php endif; ?>
</section>