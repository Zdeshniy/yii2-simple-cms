<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html;
use frontend\modules\site\AppAsset;
use yii\web\JqueryAsset;
?>

  <?php $i=0; ?>
  <?php foreach($models as $post): ?>
    <?php if($i == 0): ?>
      <div class="post_row">
    <?php endif; ?>
    <?php if($i%3 == 0):?>
      </div>
      <div class="post_row">
    <?php endif; ?>
        <div class="post_coll">
          <div class="post_item_top">
            <?=date("d.m.Y", $post['create_time']); ?> /
            <span class="icon icon_eye"><svg xmlns="http://www.w3.org/2000/svg" class="svg-icon" width="19" height="14"><path fill="currentColor" fill-rule="evenodd" clip-rule="evenodd" d="M9,4.561c-1.08,0-1.96,0.96-1.96,2.06c0,1.131,0.88,2.04,1.96,2.04 c1.061,0,1.98-0.909,1.98-2.04C10.98,5.521,10.061,4.561,9,4.561z M13.41,6.62c0-2.51-1.99-4.57-4.41-4.57S4.59,4.11,4.59,6.62 c0,2.52,1.99,4.58,4.41,4.58S13.41,9.14,13.41,6.62z M18,6.62c0,0-3.62,6.49-9,6.49S0,6.62,0,6.62s3.62-6.51,9-6.51S18,6.62,18,6.62 z"></path></svg></span>
            <?=$post['views'];?>
          </div>
          <div class="post_item_image">
            <img src="http://static.etoservis.ru/blogs/images/<?=$post['image_url'];?>" title="<?=$post['title'];?>" alt="<?=$post['title'];?>">
          </div>
          <div class="post_item_title"><a href="<?=$post['alias'];?>"><?=$post['title'];?></a></div>
          <div class="post_item_content">
            <?php
            if(strlen(strip_tags($post['content']))> 300)
              echo mb_substr(strip_tags($post['content']), 0, strpos(strip_tags($post['content']), ' ', 300));
            else
              echo strip_tags($post['content']);
            ?>
            <a href="<?=$post['alias'];?>">[...]</a>
          </div>
        </div>
        <?php $i++; ?>
  <?php endforeach;?>
  <?php if(($i-1)%3 != 0): ?>
    </div>
  <?php endif; ?>