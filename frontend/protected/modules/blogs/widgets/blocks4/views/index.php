<?php
/**
 * Основной файл представления виджета
 * @var yii\base\View $this
 * @var frontend\modules\myModule\models\Model $model
 */
use yii\helpers\Html; ?>
<div class="top_4_blocks">
  <span class="ttitle"><?=$secondaryTitle;?></span>
  <?php foreach($models as $model): ?>
  <a href="<?=$model->link;?>" title="<?=$model->title;?>">
    <?=$model->title_button;?>
  </a>
  <?php endforeach; ?>
</div>