jQuery(document).ready(function($) {
	var menu = '#topmenu',
		isTouchDevice = 'ontouchstart' in window || navigator.msMaxTouchPoints;

	if (isTouchDevice) {
		$(document).on('click', menu + ' > li > a', function(evt) {
			if ($(this).parent().hasClass('parent') && !$(this).hasClass('active')) {
				evt.preventDefault();

				$(this).addClass('active');
				$(menu).find('ul').css('display', 'none');
				$(this).parent().find('ul').css('display', 'block');
			}
		});
	} else {
		$(menu + ' > li').hover(function() {
			$(this).find('ul').css('display', 'block');
		}, function() {
			$(this).find('ul').css('display', 'none');
		});
	}
});