<?php
/**
 * Страница категории
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $model
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Menu;

$this->title = $model['title'];
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'modelId' => $model['id']
];

$images = '';
foreach ($model->images as $image) {
    $images .= Html::img($image->url, ['alt' => $image['name'], 'style' => 'float: left; width: 100px; margin: 0 10px 10px 0;']);
}

echo DetailView::widget([
	'model' => $model,
	'attributes' => [
	    'id',
	    'title',
        [
            'attribute' => 'category_id',
            'value' => $model->category['title']
        ],
	    [
            'attribute' => 'status_id',
            'value' => $model->status
        ],
        [
            'label' => 'Изображения',
            'format' => 'html',
            'value' => $images
        ],
	]
]);
