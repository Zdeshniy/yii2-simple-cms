<?php
/**
 * Страница категории
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $model
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = isset($model['title']) ? $model['title'] : 'Последняя работа';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'items' => [
        [
            'label' => '<span class="glyphicon glyphicon-pencil"></span> Изменить',
            'url' => ['update-last']
        ]
    ]
];

if ($model) {
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'gallery_id',
                'format' => 'html',
                'value' => Html::a($model->gallery['title'], ['/galleries/default/view', 'id' => $model->gallery['id']])
            ],
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => Html::img($model->thumb, ['class' => 'img-responsive'])
            ]
        ]
    ]);
} else {
    echo Html::tag('p', 'Последняя работа ещё не выбрана.');
}