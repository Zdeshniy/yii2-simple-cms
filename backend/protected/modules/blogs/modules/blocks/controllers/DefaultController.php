<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 30/09/14
 * Time: 23:31
 */

namespace backend\modules\blogs\modules\blocks\controllers;

use backend\modules\blogs\modules\blocks\models\BlocksForm;
use Yii;
use yii\web\Response;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use backend\modules\admin\components\Controller;
use common\modules\users\models\User;

class DefaultController  extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['access']['rules'][] = [
      'allow' => true,
      'roles' => [User::ROLE_MODERATOR]
    ];
    return $behaviors;
  }

  /**
   * Выводим все записи.
   */
  public function actionIndex()
  {
    $model = new BlocksForm();
    if(Yii::$app->request->post('BlocksForm', false)){
        $model->attributes = Yii::$app->request->post('BlocksForm');
        $model->save();
    }
    return $this->render('index', ['model'=>$model]);
  }
} 