<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 01/10/14
 * Time: 00:20
 */

namespace backend\modules\blogs\modules\blocks\models;

use yii\base\Model;

class BlocksForm extends Model
{
  public $title_button1;
  public $title_button2;
  public $title_button3;
  public $title_button4;

  public $title1;
  public $title2;
  public $title3;
  public $title4;

  public $link1;
  public $link2;
  public $link3;
  public $link4;

  public $id1;
  public $id2;
  public $id3;
  public $id4;

  public function __construct(){
    $models = Blocks::find()->all();
    //это сделано так как блоков планируеться всегда только 4
    foreach($models as $k=>$model){
      $this->{"title_button".($k+1)} = $model->title_button;
      $this->{"title".($k+1)} = $model->title;
      $this->{"link".($k+1)} = $model->link;
      $this->{"id".($k+1)} = $model->id;
    }
  }

  /**
   * @return array the validation rules.
   */
  public function rules()
  {
    return [
      // username and password are both required
      [[
        'title_button1',
        'title_button2',
        'title_button3',
        'title_button4',
        'title1',
        'title2',
        'title3',
        'title4',
        'link1',
        'link2',
        'link3',
        'link4',
      ],
        'required'],
    ];
  }


  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'title_button1' => 'Заголовок кнопки',
      'title_button2' => 'Заголовок кнопки',
      'title_button3' => 'Заголовок кнопки',
      'title_button4' => 'Заголовок кнопки',

      'title1' => 'Заголовок',
      'title2' => 'Заголовок',
      'title3' => 'Заголовок',
      'title4' => 'Заголовок',

      'link1' => 'Ссылка',
      'link2' => 'Ссылка',
      'link3' => 'Ссылка',
      'link4' => 'Ссылка',
    ];
  }

  public function save(){
    for($i=1; $i<=4; $i++){
      $model = Blocks::findOne($this->{"id".$i});
      $model->title_button = $this->{"title_button".$i};
      $model->title = $this->{"title".$i};
      $model->link = $this->{"link".$i};
      $model->update();
    }
  }
} 