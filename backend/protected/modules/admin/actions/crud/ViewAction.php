<?php
namespace backend\modules\admin\actions\crud;

use yii\base\InvalidConfigException;

/**
 * ViewAction экшен вывода одной записи модели.
 * 
 * Пример использования:
 * ~~~
 * public function actions()
 * {
 *     'view' => [
 *         'class' => ViewAction::className(),
 *         'model' => Post::className(),
 *         'view' => 'view'
 *     ]
 * }
 * ~~~
 */
class ViewAction extends Action
{
	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		if ($this->view === null) {
			throw new InvalidConfigException('{$this->view} is required.');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function run($id)
	{
		$this->params['model'] = $this->findModel($id);
		return $this->controller->render($this->view, $this->params);
	}
}