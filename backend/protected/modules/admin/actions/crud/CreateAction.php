<?php
namespace backend\modules\admin\actions\crud;

use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\modules\seo\models\Seo;

/**
 * CreateAction экшен создания новой записи модели.
 *
 * Пример использования:
 * ~~~
 * public function actions()
 * {
 *     'create' => [
 *         'class' => CreateAction::className(),
 *         'model' => Post::className(),
 *         'view' => 'create',
 *         'scenario' => 'admin-create'
 *     ]
 * }
 * ~~~
 */
class CreateAction extends Action
{
	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		if ($this->view === null) {
			throw new InvalidConfigException('{$this->view} is required.');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$model = new $this->model;
		if ($this->scenario !== null) {
			$model->setScenario($this->scenario);
		}

        if ($this->seo) {
            $seoModel = new Seo();
        }

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($this->seo && $seoModel->load(Yii::$app->getRequest()->post()) && $seoModel->validate()) {
                if ($seoModel->isNewRecord) {
                    $seoModel->model_id = $model->id;
                    $seoModel->model_class_id = $model->getModelClassId();
                }
                $seoModel->save(false);
            }
			return $this->controller->redirect(['update', 'id' => $model['id']]);
		} elseif (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		} else {
			$this->params['model'] = $model;
			return $this->controller->render($this->view, $this->params);
		}
	}
}
