<?php
namespace backend\modules\admin\actions\crud;

/**
 * DeleteAction экшен удаления записи модели.
 * 
 * Пример использования:
 * ~~~
 * public function actions()
 * {
 *     'batchDelete' => [
 *         'class' => UpdateAction::className(),
 *         'model' => Post::className()
 *     ]
 * }
 * ~~~
 */
class BatchDeleteAction extends Action
{
	/**
	 * @inheritdoc
	 */
	public function run(array $ids)
	{
		$models = $this->findModel($ids);
		foreach ($models as $model) {
			$model->delete();
		}
		return $this->controller->redirect(['index']);
	}
}