<?php
namespace backend\modules\materials\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\materials\models\Material;

/**
 * Общая модель поиска по постам блога
 */
class MaterialSearch extends Model
{
	public $title;
	public $parent_id;
	public $status_id;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'parent_id'], 'string'],
			[['status_id'], 'boolean'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'title' => 'Заголовок',
			'parent_id' => 'Родитель',
			'status_id' => 'Статус'
		];
	}

	public function search($params)
	{
		$query = Material::find()->orderBy('ordering ASC');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'title', true);
		$this->addCondition($query, 'status_id');
		$this->addWithCondition($query, 'parent_id', 'parent', Material::tableName() . '.title', true);
		return $dataProvider;
	}

	/**
	 * Функция добавления условий поиска.
	 * @param yii\db\Query $query Экземпляр выборки.
	 * @param string $attribute Имя отрибута по которому нужно искать.
	 * @param boolean $partialMatch Тип добавляемого сравнения. Строгое совпадение или частичное.
	 */
	protected function addCondition($query, $attribute, $partialMatch = false) 
    { 
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        $attribute = Material::tableName() . '.' . $attribute;
        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }

	/**
	 * Функция добавления условий поиска по связаным моделям.
	 * @param yii\db\Query $query Экземпляр выборки.
	 * @param string $attribute Имя отрибута с переданым значением.
	 * @param string $relation Имя связи.
	 * @param string $targetAttribute Имя удаленного отрибута по которому нужно искать.
	 * @param boolean $partialMatch Тип добавляемого сравнения. Строгое совпадение или частичное.
	 */
	protected function addWithCondition($query, $attribute, $relation, $targetAttribute, $partialMatch = false) 
    {
        $value = $this->$attribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
        	$query->innerJoinWith([$relation])
        	      ->andWhere(['like', $targetAttribute, $value]);
        } else {
            $query->innerJoinWith([$relation])
                  ->andWhere([$targetAttribute => $value]);
        }
    }
}
