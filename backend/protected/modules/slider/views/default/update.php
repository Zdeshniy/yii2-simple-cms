<?php
/**
 * Обновляем пользователя
 * @var yii\base\View $this
 * @var backend\modules\users\models\User $model
 */

use yii\helpers\Html;

$this->title = 'Обновить слайдер: ' . $model['title'];
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title)
];

echo $this->render('_form', [
	'model' => $model,
	'imageModel' => $imageModel,
	'statusArray' => $statusArray,
	'positionArray' => $positionArray
]);