<?php
/**
 * Создание пользователя
 * @var yii\base\View $this
 * @var backend\modules\users\models\User $model
 */

use yii\helpers\Html;

$this->title = 'Новый слайд';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title)
];

echo $this->render('_form', [
	'model' => $model,
	'imageModel' => $imageModel,
	'statusArray' => $statusArray,
	'positionArray' => $positionArray
]);