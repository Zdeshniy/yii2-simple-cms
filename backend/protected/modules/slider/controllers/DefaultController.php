<?php
namespace backend\modules\slider\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\AccessControl;
use yii\web\Response;
use yii\web\VerbFilter;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use backend\modules\admin\components\Controller;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\crud\CreateAction;
use backend\modules\admin\actions\crud\UpdateAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\BatchDeleteAction;
use backend\modules\admin\actions\grid\OrderingAction;
use backend\modules\slider\models\search\SliderSearch;
use common\modules\slider\models\Slider;
use common\modules\slider\models\Image;
use common\extensions\fileapi\actions\UploadAction as FileAPIUploadAction;
use common\extensions\fileapi\actions\DeleteAction as FileAPIDeleteAction;

/**
 * Основной контроллер backend-модуля [[Slider]]
 */
class DefaultController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return Slider::className();
	}

	/**
	 * @return string Класс основной поисковой модели контролера.
	 */
	public function getSearchModelClass()
	{
		return SliderSearch::className();
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
		    'index' => [
		    	'class' => IndexAction::className(),
		    	'model' => $this->modelClass,
		    	'searchModel' => $this->searchModelClass,
		    	'view' => 'index',
		    	'params' => [
		    		'statusArray' => Slider::getStatusArray()
		    	]
		    ],
		    'view' => [
		    	'class' => ViewAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'view'
		    ],
		    'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'batch-delete' => [
		    	'class' => BatchDeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'upload-temp-image' => [
		        'class' => FileAPIUploadAction::className(),
		        'path' => $this->module->tempPath(),
		        'types' => $this->module->imageAllowedExtensions
		    ],
		    'delete-temp-image' => [
		        'class' => FileAPIDeleteAction::className(),
		        'path' => $this->module->tempPath()
		    ],
		    'ordering' => [
		    	'class' => OrderingAction::className(),
		    	'model' => $this->modelClass
		    ]
		];
	}

	/**
	 * Удаляем изображение поста.
	 */
	function actionDeleteImage()
	{
		if ($id = Yii::$app->request->getDelete('id')) {
			$model = $this->findModel($id);
			$model->setScenario('delete-image');
			$model->save(false);
		} else {
			throw new HttpException(400);
		}
	}

	/**
	 * Страница создания новой модели
	 * В случае успеха, пользователь будет перенаправлен на view страницу
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Slider(['scenario' => 'admin-create']);
		$imageModel = new Image;
		$statusArray = Slider::getStatusArray();
		$positionArray = Slider::getPositionArray();

		if (Yii::$app->request->isPost) {
			$model->load($_POST);
			$models = [];
			if (($images = Yii::$app->getRequest()->post('Image')) && is_array($images)) {
				foreach ($images as $key => $value) {
					if (isset($images[$key])) {
						$image = new Image(['scenario' => 'admin-create']);
						$image->setAttributes($images[$key]);
						$models[] = $image;
					}
				}
			}
			if ($model->validate() && Model::validateMultiple($models)) {
				$model->items = $models;
				if ($model->save(false)) {
					return $this->redirect(['update', 'id' => $model['id']]);
				}
			} elseif (Yii::$app->request->isAjax) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array_merge(ActiveForm::validate($model), ActiveForm::validateMultiple($models));
			}
		}

		return $this->render('create', [
			'model' => $model,
			'imageModel' => $imageModel,
			'statusArray' => $statusArray,
			'positionArray' => $positionArray
		]);
	}

	/**
	 * Страница обновления модели
	 * В случае успеха, пользователь будет перенаправлен на view страницу
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->setScenario('admin-update');
		$imageModel = new Image;
		$statusArray = Slider::getStatusArray();
		$positionArray = Slider::getPositionArray();

		if (Yii::$app->request->isPost) {
			$model->load($_POST);
			$items = $models = [];
			if (($images = Yii::$app->getRequest()->post('Image')) && is_array($images)) {
				foreach ($images as $key => $value) {
					if (isset($value['name'])) {
						$image = new Image(['scenario' => 'admin-create']);
						$image->setAttributes($images[$key]);
						$items[] = $image;
						$models[$key] = $image;
					} elseif (isset($model->images[$key])) {
						$image = $model->images[$key];
						$image->setScenario('admin-update');
						$image->setAttributes($images[$key]);
						$models[$key] = $image;
					}
				}
			}
			if ($model->validate() && Model::validateMultiple($models)) {
				if (!empty($items)) {
					$model->items = $items;
				}
				if ($model->save(false)) {
					foreach ($model->images as $item) {
						$item->save(false);
					}
					return $this->redirect(['update', 'id' => $model['id']]);
				}
			} elseif (Yii::$app->request->isAjax) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				return array_merge(ActiveForm::validate($model), ActiveForm::validateMultiple($models));
			}
		}

		return $this->render('update', [
			'model' => $model,
			'imageModel' => $imageModel,
			'statusArray' => $statusArray,
			'positionArray' => $positionArray
		]);
	}
}