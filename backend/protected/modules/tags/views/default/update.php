<?php
/**
 * Представление обновления тэга.
 * @var yii\base\View $this
 * @var common\modules\blogs\models\Post $model Модель
 */

use yii\helpers\Html;
use yii\widgets\Menu;

$this->title = 'Обновить тэг: ' . $model['name'];

echo $this->render('_form', [
	'model' => $model
]);