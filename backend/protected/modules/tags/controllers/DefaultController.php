<?php
namespace backend\modules\tags\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use backend\modules\admin\components\Controller;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\crud\CreateAction;
use backend\modules\admin\actions\crud\UpdateAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\BatchDeleteAction;
use backend\modules\admin\actions\grid\OrderingAction;
use backend\modules\tags\models\search\TagSearch;
use common\modules\tags\models\Tag;

/**
 * Основной контроллер backend-модуля [[Tags]]
 */
class DefaultController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return Tag::className();
	}

	/**
	 * @return string Класс основной поисковой модели контролера.
	 */
	public function getSearchModelClass()
	{
		return TagSearch::className();
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
		    'index' => [
		    	'class' => IndexAction::className(),
		    	'model' => $this->modelClass,
		    	'searchModel' => $this->searchModelClass,
		    	'view' => 'index'
		    ],
		    'view' => [
		    	'class' => ViewAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'view'
		    ],
		    'create' => [
		    	'class' => CreateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'create',
		    	'scenario' => 'admin-create',
                'seo' => true
		    ],
		    'update' => [
		    	'class' => UpdateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'update',
		    	'scenario' => 'admin-update',
                'seo' => true,
		    ],
		    'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'batch-delete' => [
		    	'class' => BatchDeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'ordering' => [
		    	'class' => OrderingAction::className(),
		    	'model' => $this->modelClass
		    ]
		];
	}
}
