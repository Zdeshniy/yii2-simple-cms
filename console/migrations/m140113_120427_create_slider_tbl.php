<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Миграция которая создает таблицу слайдера в БД.
 */
class m140113_120427_create_slider_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Создаём таблицу слайдера
		$this->createTable('{{%slider}}', [
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING . '(255) NOT NULL',
			'content' => Schema::TYPE_TEXT . ' NOT NULL',
			'position_id' => 'enum("tl", "tm", "tr", "ml", "mm", "mr", "bl", "bm", "br") NOT NULL DEFAULT "tl"',
			'ordering' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1'
		], $tableOptions);

		// Создаём таблицу изображений слайдера
		$this->createTable('{{%slider_image}}', [
			'id' => Schema::TYPE_PK,
			'slider_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'name' => Schema::TYPE_STRING . '(64) NOT NULL'
		], $tableOptions);

		$this->addForeignKey('FK_slider_image_slider_id', '{{%slider_image}}', 'slider_id', '{{%slider}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('{{%slider_image}}');
		$this->dropTable('{{%slider}}');
	}
}
