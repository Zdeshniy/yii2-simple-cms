<?php

use yii\db\Schema;
use yii\db\Migration;
use common\modules\tags\helpers\TagsHelper;
use common\modules\pages\models\Page;

/**
 * Миграция которая создает все таблицы БД которые относятся к страницам
 */
class m131211_154421_create_pages_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Добавляем запись в таблицу моделей
		$modelClass = Page::className();
		$modelClassId = TagsHelper::crcClassName($modelClass);
		$modelClass = addslashes($modelClass);
		$this->execute("INSERT INTO {{%models}} (`model_class`, `model_class_id`) VALUES ('$modelClass', '$modelClassId')");

		// Создаём таблицу страниц
		$this->createTable('{{%pages}}', array(
			'id' => Schema::TYPE_PK,
			'parent_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'title' => Schema::TYPE_STRING . '(100) NOT NULL',
			'alias' => Schema::TYPE_STRING . '(100) NOT NULL',
			'content' => 'longtext NOT NULL',
			'image_url' => Schema::TYPE_STRING . '(64) NOT NULL',
			'service' => 'tinyint(1) NOT NULL DEFAULT 0',
			'info' => 'tinyint(1) NOT NULL DEFAULT 0',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1',
			'create_time' => Schema::TYPE_INTEGER . ' NOT NULL',
			'update_time' => Schema::TYPE_INTEGER . ' NOT NULL',
		), $tableOptions);

		$this->createIndex('parent_id', '{{%pages}}', 'parent_id');
		$this->createIndex('alias', '{{%pages}}', 'alias');
		$this->createIndex('status_id', '{{%pages}}', 'status_id');
		$this->createIndex('create_time', '{{%pages}}', 'create_time');
		$this->createIndex('update_time', '{{%pages}}', 'update_time');
	}

	public function down()
	{
		$this->dropTable('{{%pages}}');
	}
}
