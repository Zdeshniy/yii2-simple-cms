phpinfo()
PHP Version => 5.2.17

System => FreeBSD v76.valuehost.ru 9.3-BETA1 FreeBSD 9.3-BETA1 #2 r266969: Mon Jun  2 13:12:02 MSK 2014     ppb@v76.valuehost.ru:/usr/obj/usr/src/sys/OMNIPOTENT amd64
Build Date => Sep 24 2012 13:10:27
Configure Command =>  './configure'  '--enable-dbx' '--with-pdflib' '--enable-translit' '--enable-dbase' '--with-apxs=/usr/local/apache/sbin/apxs' '--prefix=/usr/local/php5.2' '--with-mysql' '--with-mysqli=/usr/local/bin/mysql_config' '--with-pdo-mysql=/usr/local' '--with-config-file-path=/usr/local/php5.2/etc' '--with-gd' '--with-freetype-dir=/usr/local' '--enable-gd-native-ttf' '--enable-gd-jis-conv' '--with-zlib' '--with-jpeg-dir=/usr/local' '--with-png-dir=/usr/local' '--with-ldap=/usr/local' '--with-openssl=/usr' '--enable-bcmath' '--enable-calendar' '--with-curl=/usr/local' '--enable-exif' '--enable-ftp' '--with-t1lib=/usr/local' '--with-gettext=/usr/local' '--with-gmp=/usr/local/gmp4' '--enable-sysvsem' '--enable-dba=shared' '--with-pear' '--disable-ipv6' '--with-layout=GNU' '--with-mssql' '--enable-mbstring' '--with-mcrypt=/usr/local' '--enable-sockets' '--enable-sysvshm' '--with-pgsql' '--with-pdo-pgsql' '--enable-zip' '--with-bz2' '--with-xmlrpc' '--with-iconv-dir=/usr/local' '--enable-soap' '--with-mhash' '--with-imap=/usr/local' '--with-imap-ssl' '--with-xsl=/usr/local'
Server API => Command Line Interface
Virtual Directory Support => disabled
Configuration File (php.ini) Path => /usr/local/php5.2/etc
Loaded Configuration File => /usr/local/php5.2/etc/php.ini
Scan this dir for additional .ini files => (none)
additional .ini files parsed => (none)
PHP API => 20041225
PHP Extension => 20060613
Zend Extension => 220060519
Debug Build => no
Thread Safety => disabled
Zend Memory Manager => enabled
IPv6 Support => disabled
Registered PHP Streams => https, ftps, compress.zlib, compress.bzip2, php, file, data, http, ftp, zip  
Registered Stream Socket Transports => tcp, udp, unix, udg, ssl, sslv3, sslv2, tls
Registered Stream Filters => zlib.*, bzip2.*, convert.iconv.*, string.rot13, string.toupper, string.tolower, string.strip_tags, convert.*, consumed


This program makes use of the Zend Scripting Language Engine:
Zend Engine v2.2.0, Copyright (c) 1998-2010 Zend Technologies
    with eAccelerator v1.0-dev, Copyright (c) 2004-2012 eAccelerator, by eAccelerator
    with the ionCube PHP Loader v4.5.0, Copyright (c) 2002-2013, by ionCube Ltd., and
    with Zend Extension Manager v1.2.2, Copyright (c) 2003-2007, by Zend Technologies
    with Zend Optimizer v3.3.0, Copyright (c) 1998-2007, by Zend Technologies


 _______________________________________________________________________


Configuration

PHP Core

Directive => Local Value => Master Value
allow_call_time_pass_reference => On => On
allow_url_fopen => On => On
allow_url_include => Off => Off
always_populate_raw_post_data => Off => Off
arg_separator.input => & => &
arg_separator.output => & => &
asp_tags => Off => Off
auto_append_file => no value => no value
auto_globals_jit => On => On
auto_prepend_file => no value => no value
browscap => no value => no value
default_charset => no value => no value
default_mimetype => text/html => text/html
define_syslog_variables => Off => Off
disable_classes => no value => no value
disable_functions => no value => no value
display_errors => STDOUT => STDOUT
display_startup_errors => Off => Off
doc_root => no value => no value
docref_ext => no value => no value
docref_root => no value => no value
enable_dl => On => On
error_append_string => no value => no value
error_log => no value => no value
error_prepend_string => no value => no value
error_reporting => 6135 => 6135
expose_php => On => On
extension_dir => /usr/local/php5.2/lib/php/20060613 => /usr/local/php5.2/lib/php/20060613
file_uploads => On => On
highlight.bg => <font style="color: #FFFFFF">#FFFFFF</font> => <font style="color: #FFFFFF">#FFFFFF</font>
highlight.comment => <font style="color: #FF8000">#FF8000</font> => <font style="color: #FF8000">#FF8000</font>
highlight.default => <font style="color: #0000BB">#0000BB</font> => <font style="color: #0000BB">#0000BB</font>
highlight.html => <font style="color: #000000">#000000</font> => <font style="color: #000000">#000000</font>
highlight.keyword => <font style="color: #007700">#007700</font> => <font style="color: #007700">#007700</font>
highlight.string => <font style="color: #DD0000">#DD0000</font> => <font style="color: #DD0000">#DD0000</font>
html_errors => Off => Off
ignore_repeated_errors => Off => Off
ignore_repeated_source => Off => Off
ignore_user_abort => Off => Off
implicit_flush => On => On
include_path => .:/usr/local/php5.2/share/pear => .:/usr/local/php5.2/share/pear
log_errors => Off => Off
log_errors_max_len => 1024 => 1024
magic_quotes_gpc => On => On
magic_quotes_runtime => Off => Off
magic_quotes_sybase => Off => Off
mail.force_extra_parameters => no value => no value
max_execution_time => 0 => 0
max_file_uploads => 20 => 20
max_input_nesting_level => 64 => 64
max_input_time => -1 => -1
memory_limit => 64M => 64M
open_basedir => no value => no value
output_buffering => 0 => 0
output_handler => no value => no value
post_max_size => 8M => 8M
precision => 12 => 12
realpath_cache_size => 4096k => 4096k
realpath_cache_ttl => 120 => 120
register_argc_argv => On => On
register_globals => On => On
register_long_arrays => On => On
report_memleaks => On => On
report_zend_debug => Off => Off
safe_mode => Off => Off
safe_mode_exec_dir => no value => no value
safe_mode_gid => Off => Off
safe_mode_include_dir => no value => no value
sendmail_from => no value => no value
sendmail_path => /usr/sbin/sendmail -t -i  => /usr/sbin/sendmail -t -i 
serialize_precision => 100 => 100
short_open_tag => On => On
SMTP => localhost => localhost
smtp_port => 25 => 25
sql.safe_mode => Off => Off
track_errors => Off => Off
unserialize_callback_func => no value => no value
upload_max_filesize => 2M => 2M
upload_tmp_dir => no value => no value
user_dir => no value => no value
variables_order => EGPCS => EGPCS
xmlrpc_error_number => 0 => 0
xmlrpc_errors => Off => Off
y2k_compliance => On => On
zend.ze1_compatibility_mode => Off => Off

bcmath

BCMath support => enabled

bz2

BZip2 Support => Enabled
Stream Wrapper support => compress.bz2://
Stream Filter support => bzip2.decompress, bzip2.compress
BZip2 Version => 1.0.6, 6-Sept-2010

calendar

Calendar support => enabled

ctype

ctype functions => enabled

curl

cURL support => enabled
cURL Information => libcurl/7.21.3 OpenSSL/0.9.8q zlib/1.2.5

date

date/time support => enabled
"Olson" Timezone Database Version => 2012.9
Timezone Database => external
Default timezone => Europe/Moscow

Directive => Local Value => Master Value
date.default_latitude => 31.7667 => 31.7667
date.default_longitude => 35.2333 => 35.2333
date.sunrise_zenith => 90.583333 => 90.583333
date.sunset_zenith => 90.583333 => 90.583333
date.timezone => Europe/Moscow => Europe/Moscow

dbx

dbx support => enabled
dbx version => 1.1.1-dev
supported databases => MySQL
ODBC
PostgreSQL
Microsoft SQL Server
FrontBase
Oracle 8 (oci8)
Sybase-CT
SQLite

Directive => Local Value => Master Value
dbx.colnames_case => unchanged => unchanged

dom

DOM/XML => enabled
DOM/XML API Version => 20031129
libxml Version => 2.7.8
HTML Support => enabled
XPath Support => enabled
XPointer Support => enabled
Schema Support => enabled
RelaxNG Support => enabled

eAccelerator

eAccelerator support => enabled
Version => 1.0-dev
Caching Enabled => false
Optimizer Enabled => false
Check mtime Enabled => false

Directive => Local Value => Master Value
eaccelerator.allowed_admin_path => no value => no value
eaccelerator.cache_dir => /tmp/eaccelerator52 => /tmp/eaccelerator52
eaccelerator.check_mtime => 1 => 1
eaccelerator.debug => 0 => 0
eaccelerator.enable => 1 => 1
eaccelerator.filter => no value => no value
eaccelerator.log_file => no value => no value
eaccelerator.optimizer => 1 => 1
eaccelerator.shm_only => 0 => 0
eaccelerator.shm_prune_period => 0 => 0
eaccelerator.shm_size => 16 => 16
eaccelerator.shm_ttl => 3600 => 3600

exif

EXIF Support => enabled
EXIF Version => 1.4 $Id: exif.c 293036 2010-01-03 09:23:27Z sebastian $
Supported EXIF Version => 0220
Supported filetypes => JPEG,TIFF

filter

Input Validation and Filtering => enabled
Revision => $Revision: 298196 $

Directive => Local Value => Master Value
filter.default => unsafe_raw => unsafe_raw
filter.default_flags => no value => no value

ftp

FTP support => enabled

gd

GD Support => enabled
GD Version => bundled (2.0.34 compatible)
FreeType Support => enabled
FreeType Linkage => with freetype
FreeType Version => 2.4.7
T1Lib Support => enabled
GIF Read Support => enabled
GIF Create Support => enabled
JPG Support => enabled
PNG Support => enabled
WBMP Support => enabled
XBM Support => enabled
JIS-mapped Japanese Font Support => enabled

gettext

GetText Support => enabled

gmp

gmp support => enabled
GMP version => 5.0.2

hash

hash support => enabled
Hashing Engines => md2 md4 md5 sha1 sha256 sha384 sha512 ripemd128 ripemd160 ripemd256 ripemd320 whirlpool tiger128,3 tiger160,3 tiger192,3 tiger128,4 tiger160,4 tiger192,4 snefru gost adler32 crc32 crc32b haval128,3 haval160,3 haval192,3 haval224,3 haval256,3 haval128,4 haval160,4 haval192,4 haval224,4 haval256,4 haval128,5 haval160,5 haval192,5 haval224,5 haval256,5 

iconv

iconv support => enabled
iconv implementation => libiconv
iconv library version => 1.13

Directive => Local Value => Master Value
iconv.input_encoding => ISO-8859-1 => ISO-8859-1
iconv.internal_encoding => ISO-8859-1 => ISO-8859-1
iconv.output_encoding => ISO-8859-1 => ISO-8859-1

imap

IMAP c-Client Version => 2007f
SSL Support => enabled

json

json support => enabled
json version => 1.2.1

ldap

LDAP Support => enabled
RCS Version => $Id: ldap.c 293036 2010-01-03 09:23:27Z sebastian $
Total Links => 0/unlimited
API Version => 3001
Vendor Name => OpenLDAP
Vendor Version => 20426

libxml

libXML support => active
libXML Version => 2.7.8
libXML streams => enabled

mbstring

Multibyte Support => enabled
Multibyte string engine => libmbfl
Multibyte (japanese) regex support => enabled
Multibyte regex (oniguruma) version => 4.4.4
Multibyte regex (oniguruma) backtrack check => On

mbstring extension makes use of "streamable kanji code filter and converter", which is distributed under the GNU Lesser General Public License version 2.1.

Directive => Local Value => Master Value
mbstring.detect_order => no value => no value
mbstring.encoding_translation => Off => Off
mbstring.func_overload => 0 => 0
mbstring.http_input => pass => pass
mbstring.http_output => pass => pass
mbstring.internal_encoding => UTF-8 => UTF-8
mbstring.language => neutral => neutral
mbstring.strict_detection => Off => Off
mbstring.substitute_character => no value => no value

mcrypt

mcrypt support => enabled
Version => 2.5.8
Api No => 20021217
Supported ciphers => cast-128 gost rijndael-128 twofish arcfour cast-256 loki97 rijndael-192 saferplus wake blowfish-compat des rijndael-256 serpent xtea blowfish enigma rc2 tripledes 
Supported modes => cbc cfb ctr ecb ncfb nofb ofb stream 

Directive => Local Value => Master Value
mcrypt.algorithms_dir => no value => no value
mcrypt.modes_dir => no value => no value

mhash

MHASH support => Enabled
MHASH API Version => 20060101

mssql

MSSQL Support => enabled
Active Persistent Links => 0
Active Links => 0
Library version => FreeTDS

Directive => Local Value => Master Value
mssql.allow_persistent => On => On
mssql.batchsize => 0 => 0
mssql.charset => no value => no value
mssql.compatability_mode => Off => Off
mssql.connect_timeout => 5 => 5
mssql.datetimeconvert => On => On
mssql.max_links => Unlimited => Unlimited
mssql.max_persistent => Unlimited => Unlimited
mssql.max_procs => Unlimited => Unlimited
mssql.min_error_severity => 10 => 10
mssql.min_message_severity => 10 => 10
mssql.secure_connection => Off => Off
mssql.textlimit => Server default => Server default
mssql.textsize => Server default => Server default
mssql.timeout => 60 => 60

mysql

MySQL Support => enabled
Active Persistent Links => 0
Active Links => 0
Client API version => 5.5.17
MYSQL_MODULE_TYPE => external
MYSQL_SOCKET => /tmp/mysql.sock
MYSQL_INCLUDE => -I/usr/local/include/mysql
MYSQL_LIBS => -L/usr/local/lib/mysql -lmysqlclient 

Directive => Local Value => Master Value
mysql.allow_persistent => On => On
mysql.connect_timeout => 60 => 60
mysql.default_host => no value => no value
mysql.default_password => no value => no value
mysql.default_port => no value => no value
mysql.default_socket => no value => no value
mysql.default_user => no value => no value
mysql.max_links => Unlimited => Unlimited
mysql.max_persistent => Unlimited => Unlimited
mysql.trace_mode => Off => Off

mysqli

MysqlI Support => enabled
Client API library version => 5.5.17
Client API header version => 5.5.17
MYSQLI_SOCKET => /tmp/mysql.sock

Directive => Local Value => Master Value
mysqli.default_host => no value => no value
mysqli.default_port => 3306 => 3306
mysqli.default_pw => no value => no value
mysqli.default_socket => no value => no value
mysqli.default_user => no value => no value
mysqli.max_links => Unlimited => Unlimited
mysqli.reconnect => Off => Off

openssl

OpenSSL support => enabled
OpenSSL Version => OpenSSL 0.9.8q 2 Dec 2010

pcre

PCRE (Perl Compatible Regular Expressions) Support => enabled
PCRE Library Version => 8.02 2010-03-19

Directive => Local Value => Master Value
pcre.backtrack_limit => 100000 => 100000
pcre.recursion_limit => 100000 => 100000

pdf

PDF Support => enabled
PDFlib GmbH Version => 7.0.5p3
PECL Version => 2.1.9
Revision => $Revision: 325992 $

PDO

PDO support => enabled
PDO drivers => mysql, pgsql, sqlite, sqlite2

pdo_mysql

PDO Driver for MySQL, client library version => 5.5.17

pdo_pgsql

PDO Driver for PostgreSQL => enabled
PostgreSQL(libpq) Version => 8.3.16
Module version => 1.0.2
Revision =>  $Id: pdo_pgsql.c 293036 2010-01-03 09:23:27Z sebastian $ 

pdo_sqlite

PDO Driver for SQLite 3.x => enabled
PECL Module version => (bundled) 1.0.1 $Id: pdo_sqlite.c 293036 2010-01-03 09:23:27Z sebastian $
SQLite Library => 3.3.7

pgsql

PostgreSQL Support => enabled
PostgreSQL(libpq) Version => 8.3.16
Multibyte character support => enabled
SSL support => enabled
Active Persistent Links => 0
Active Links => 0

Directive => Local Value => Master Value
pgsql.allow_persistent => On => On
pgsql.auto_reset_persistent => Off => Off
pgsql.ignore_notice => Off => Off
pgsql.log_notice => Off => Off
pgsql.max_links => Unlimited => Unlimited
pgsql.max_persistent => Unlimited => Unlimited

posix

Revision => $Revision: 293036 $

Reflection

Reflection => enabled
Version => $Id: php_reflection.c 300129 2010-06-03 00:43:37Z felipe $

session

Session Support => enabled
Registered save handlers => files user sqlite 
Registered serializer handlers => php php_binary 

Directive => Local Value => Master Value
session.auto_start => Off => Off
session.bug_compat_42 => On => On
session.bug_compat_warn => On => On
session.cache_expire => 180 => 180
session.cache_limiter => nocache => nocache
session.cookie_domain => no value => no value
session.cookie_httponly => Off => Off
session.cookie_lifetime => 0 => 0
session.cookie_path => / => /
session.cookie_secure => Off => Off
session.entropy_file => no value => no value
session.entropy_length => 0 => 0
session.gc_divisor => 100 => 100
session.gc_maxlifetime => 1440 => 1440
session.gc_probability => 1 => 1
session.hash_bits_per_character => 4 => 4
session.hash_function => 0 => 0
session.name => PHPSESSID => PHPSESSID
session.referer_check => no value => no value
session.save_handler => files => files
session.save_path => no value => no value
session.serialize_handler => php => php
session.use_cookies => On => On
session.use_only_cookies => Off => Off
session.use_trans_sid => 0 => 0

SimpleXML

Simplexml support => enabled
Revision => $Revision: 299016 $
Schema support => enabled

soap

Soap Client => enabled
Soap Server => enabled

Directive => Local Value => Master Value
soap.wsdl_cache => 1 => 1
soap.wsdl_cache_dir => /tmp => /tmp
soap.wsdl_cache_enabled => 1 => 1
soap.wsdl_cache_limit => 5 => 5
soap.wsdl_cache_ttl => 86400 => 86400

sockets

Sockets Support => enabled

SPL

SPL support => enabled
Interfaces => Countable, OuterIterator, RecursiveIterator, SeekableIterator, SplObserver, SplSubject
Classes => AppendIterator, ArrayIterator, ArrayObject, BadFunctionCallException, BadMethodCallException, CachingIterator, DirectoryIterator, DomainException, EmptyIterator, FilterIterator, InfiniteIterator, InvalidArgumentException, IteratorIterator, LengthException, LimitIterator, LogicException, NoRewindIterator, OutOfBoundsException, OutOfRangeException, OverflowException, ParentIterator, RangeException, RecursiveArrayIterator, RecursiveCachingIterator, RecursiveDirectoryIterator, RecursiveFilterIterator, RecursiveIteratorIterator, RecursiveRegexIterator, RegexIterator, RuntimeException, SimpleXMLIterator, SplFileInfo, SplFileObject, SplObjectStorage, SplTempFileObject, UnderflowException, UnexpectedValueException

SQLite

SQLite support => enabled
PECL Module version => 2.0-dev $Id: sqlite.c 298697 2010-04-28 12:10:10Z iliaa $
SQLite Library => 2.8.17
SQLite Encoding => iso8859

Directive => Local Value => Master Value
sqlite.assoc_case => 0 => 0

standard

Regex Library => Bundled library enabled
Dynamic Library Support => enabled
Path to sendmail => /usr/sbin/sendmail -t -i 

Directive => Local Value => Master Value
assert.active => 1 => 1
assert.bail => 0 => 0
assert.callback => no value => no value
assert.quiet_eval => 0 => 0
assert.warning => 1 => 1
auto_detect_line_endings => 0 => 0
default_socket_timeout => 60 => 60
safe_mode_allowed_env_vars => PHP_ => PHP_
safe_mode_protected_env_vars => LD_LIBRARY_PATH => LD_LIBRARY_PATH
url_rewriter.tags => a=href,area=href,frame=src,input=src,form=,fieldset= => a=href,area=href,frame=src,input=src,form=,fieldset=
user_agent => no value => no value

timezonedb

Alternative Timezone Database => enabled
Timezone Database Version => 2012.9

tokenizer

Tokenizer Support => enabled

translit

Transliteration support => enabled
Version => 0.6.0

xml

XML Support => active
XML Namespace Support => active
libxml2 Version => 2.7.8

xmlreader

XMLReader => enabled

xmlrpc

core library version => xmlrpc-epi v. 0.51
php extension version => 0.51
author => Dan Libby
homepage => http://xmlrpc-epi.sourceforge.net
open sourced by => Epinions.com

xmlwriter

XMLWriter => enabled

xsl

XSL => enabled
libxslt Version => 1.1.26
libxslt compiled against libxml Version => 2.7.8
EXSLT => enabled
libexslt Version => 1.1.26

Zend Optimizer

Optimization Pass 1 => enabled
Optimization Pass 2 => enabled
Optimization Pass 3 => enabled
Optimization Pass 4 => enabled
Optimization Pass 9 => disabled
Zend Loader => enabled
License Path =>  
Obfuscation level => 3

zip

Zip => enabled
Extension Version => $Id: php_zip.c 305848 2010-11-30 11:04:06Z pajoye $
Zip version => 1.8.11
Libzip version => 0.9.0

zlib

ZLib Support => enabled
Stream Wrapper support => compress.zlib://
Stream Filter support => zlib.inflate, zlib.deflate
Compiled Version => 1.2.5
Linked Version => 1.2.5

Directive => Local Value => Master Value
zlib.output_compression => Off => Off
zlib.output_compression_level => -1 => -1
zlib.output_handler => no value => no value

Additional Modules

Module Name
dbase
ionCube Loader
sysvsem
sysvshm

Environment

Variable => Value
MM_CHARSET => UTF-8
SHELL => /usr/local/bin/bash
TERM => xterm
SSH_CLIENT => 80.94.251.105 52566 22
SSH_TTY => /dev/pts/2
LC_ALL => en_US.UTF-8
USER => ixakep3
FTP_PASSIVE_MODE => 1
PATH => /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
MAIL => /var/mail/ixakep3
BLOCKSIZE => K
PWD => /pub/home/ixakep3/new
LANG => en_US.UTF-8
SHLVL => 1
HOME => /pub/home/ixakep3
LOGNAME => ixakep3
SSH_CONNECTION => 80.94.251.105 52566 217.112.35.75 22
_ => /usr/local/bin/php
OLDPWD => /pub/home/ixakep3

PHP Variables

Variable => Value
PHP_SELF =>  
_SERVER["MM_CHARSET"] => UTF-8
_SERVER["SHELL"] => /usr/local/bin/bash
_SERVER["TERM"] => xterm
_SERVER["SSH_CLIENT"] => 80.94.251.105 52566 22
_SERVER["SSH_TTY"] => /dev/pts/2
_SERVER["LC_ALL"] => en_US.UTF-8
_SERVER["USER"] => ixakep3
_SERVER["FTP_PASSIVE_MODE"] => 1
_SERVER["PATH"] => /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
_SERVER["MAIL"] => /var/mail/ixakep3
_SERVER["BLOCKSIZE"] => K
_SERVER["PWD"] => /pub/home/ixakep3/new
_SERVER["LANG"] => en_US.UTF-8
_SERVER["SHLVL"] => 1
_SERVER["HOME"] => /pub/home/ixakep3
_SERVER["LOGNAME"] => ixakep3
_SERVER["SSH_CONNECTION"] => 80.94.251.105 52566 217.112.35.75 22
_SERVER["_"] => /usr/local/bin/php
_SERVER["OLDPWD"] => /pub/home/ixakep3
_SERVER["PHP_SELF"] => 
_SERVER["SCRIPT_NAME"] => 
_SERVER["SCRIPT_FILENAME"] => 
_SERVER["PATH_TRANSLATED"] => 
_SERVER["DOCUMENT_ROOT"] => 
_SERVER["REQUEST_TIME"] => 1408990924
_SERVER["argv"] => Array
(
)

_SERVER["argc"] => 0
_ENV["MM_CHARSET"] => UTF-8
_ENV["SHELL"] => /usr/local/bin/bash
_ENV["TERM"] => xterm
_ENV["SSH_CLIENT"] => 80.94.251.105 52566 22
_ENV["SSH_TTY"] => /dev/pts/2
_ENV["LC_ALL"] => en_US.UTF-8
_ENV["USER"] => ixakep3
_ENV["FTP_PASSIVE_MODE"] => 1
_ENV["PATH"] => /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
_ENV["MAIL"] => /var/mail/ixakep3
_ENV["BLOCKSIZE"] => K
_ENV["PWD"] => /pub/home/ixakep3/new
_ENV["LANG"] => en_US.UTF-8
_ENV["SHLVL"] => 1
_ENV["HOME"] => /pub/home/ixakep3
_ENV["LOGNAME"] => ixakep3
_ENV["SSH_CONNECTION"] => 80.94.251.105 52566 217.112.35.75 22
_ENV["_"] => /usr/local/bin/php
_ENV["OLDPWD"] => /pub/home/ixakep3

PHP License
This program is free software; you can redistribute it and/or modify
it under the terms of the PHP License as published by the PHP Group
and included in the distribution in the file:  LICENSE

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

If you did not receive a copy of the PHP license, or have any
questions about PHP licensing, please contact license@php.net.
