<?php
namespace common\modules\pages\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\tags\behaviors\TagBehavior;
use common\modules\seo\behaviors\SeoBehavior;
use common\behaviors\PurifierBehavior;
use common\behaviors\TransliterateBehavior;
use common\modules\users\models\User;
use common\modules\pages\models\query\PageQuery;

/**
 * Модуль таблицы {{%pages}}
 *
 * @property integer $id ID
 * @property string $parent_id ID родительской страницы
 * @property string $title Заголовок
 * @property string $alias Алиас
 * @property string $content Контент
 * @property boolean $status_id Статус
 * @property integer $create_time Дата создания
 * @property integer $update_time Дата обновления
 */
class Page extends ActiveRecord
{
	/**
	 * Статусы записей модели
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * Определяем значение тех страниц которые являются услугами.
	 */
	const ISNT_SERVICE = 0;
	const IS_SERVICE = 1;

	/**
	 * Определяем значение тех страниц которые нужно выводить в блоке информация.
	 */
	const ISNT_INFO = 0;
	const IS_INFO = 1;

	/**
	 * @var array Массив всех доступных родительских страниц
	 */
	protected $_parentsArray;

	/**
	 * @var string Изображение страницы.
	 */
	protected $_image;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%pages}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new PageQuery(get_called_class());
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		$behaviors = [
			'timestampBehavior' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['create_time', 'update_time'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
				]
			],
			'transliterateBehavior' => [
			    'class' => TransliterateBehavior::className(),
			    'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['title' => 'alias'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title' => 'alias']
				]
			],
			'uploadBehavior' => [
				'class' => UploadBehavior::className(),
				'attributes' => ['image_url'],
				'scenarios' => ['admin-create', 'admin-update'],
				'deleteScenarios' => [
				    'image_url' => 'delete-image',
				],
				'path' => [
				    'image_url' => Yii::$app->getModule('pages')->imagePath()
				],
				'tempPath' => [
				    'image_url' => Yii::$app->getModule('pages')->imageTempPath()
				]
			],
			'seoBehavior' => [
				'class' => SeoBehavior::className()
			],
			'tagBehavior' => [
				'class' => TagBehavior::className()
			]
		];
		if (!Yii::$app->getUser()->checkAccess(User::ROLE_SUPERADMIN)) {
			$behaviors['purifierBehavior'] = [
			    'class' => PurifierBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['content'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['content'],
				],
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['title'],
				]
			];
		}
		return $behaviors;
	}

	/**
	 * Выбор запись по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор запись по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * Выборка пользователя по [[alias]]
	 * @param string $alias
	 */
	public static function findByAlias($alias)
	{
		return static::find()->where('alias = :alias', [':alias' => $alias])->one();
	}

	/**
	 * @return string|null Полный URL адрес до изображения поста.
	 */
	public function getImage()
	{
		if ($this->_image === null && $this->image_url) {
			$this->_image = Yii::$app->getModule('pages')->imageUrl($this->image_url);
		}
		return $this->_image;
	}

	/**
	 * @return array Массив с статусами страниц
	 */
	public static function getStatusArray()
	{
		return [
		    self::STATUS_PUBLISHED => 'Опубликованно',
		    self::STATUS_UNPUBLISHED => 'Неопубликованно'
		];
	}

	/**
	 * @return array Массив всех доступных родительских страниц
	 */
	public function getParentsArray($id = null)
	{
		$query = self::find()->where(['parent_id' => 0]);
		if ($id !== null) {
			$query->andWhere('id <> :id', [':id' => $id]);
		}
		$parentsArray = ArrayHelper::map($query->all(), 'id', 'title');

		return $parentsArray;
	}

	/**
	 * @return string Читабельный статус поста
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'content'], 'required'],
			[['title', 'alias'], 'string', 'max' => 100],
			['alias', 'match', 'pattern' => '/^[a-z0-9_-]+$/i'],
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED],
			['parent_id', 'validateParent'],
			['service', 'in', 'range' => [self::IS_SERVICE, self::ISNT_SERVICE]],
			['info', 'in', 'range' => [self::ISNT_INFO, self::IS_INFO]]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['title', 'alias', 'content', 'parent_id', 'status_id', 'service', 'info', 'image_url', 'tagArray'],
			'admin-update' => ['title', 'alias', 'content', 'parent_id', 'status_id', 'service', 'info', 'image_url', 'tagArray']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'parent_id' => 'Родительская страница',
			'title' => 'Заголовок',
			'alias' => 'Алиас',
			'content' => 'Текст',
			'status_id' => 'Статус',
			'service' => 'Выводить на главной в блоке "Услуги"',
			'info' => 'Выводить на главной в блоке "Статьи/информация"',
			'image_url' => 'Изображение',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
			'tagArray' => 'Тэги'
		];
	}

	/**
	 * Валидация родительской страницы
	 * В правилах модели метод назначен как валидатор атрибута модели
	 * @return boolean
	 */
	public function validateParent()
	{
		$parentsArray = $this->getParentsArray();
		if ($this->parent_id && !isset($parentsArray[$this->parent_id])) {
			$this->addError('parent_id', 'Неправильная родительская страница');
		}
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getParent()
	{
		return $this->hasOne(self::className(), ['id' => 'parent_id'])->from(self::tableName() . ' parent');
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (!$this->parent_id) {
				$this->parent_id = 0;
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			if ($models = self::find()->where(['parent_id' => $this->id])->all()) {
				foreach ($models as $model) {
					$model->delete();
				}
			}
			return true;
		} else {
			return false;
		}
	}
}
