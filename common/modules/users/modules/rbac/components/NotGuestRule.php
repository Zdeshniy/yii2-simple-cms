<?php 
namespace common\modules\users\modules\rbac\components;

use Yii;
use yii\rbac\Rule;

class NotGuestRule extends Rule
{
	public $name = 'NotGuestRule';
	
    public function execute($params, $data)
    {
        return !Yii::$app->user->isGuest;
    }
}