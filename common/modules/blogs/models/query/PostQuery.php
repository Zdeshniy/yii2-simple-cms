<?php
namespace common\modules\blogs\models\query;

use yii\db\ActiveQuery;
use common\modules\blogs\models\Post;

/**
 * Class PostQuery
 * Класс кастомных запросов модели [[Post]]
 */
class PostQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованые записи.
	 * @param ActiveQuery $query
	 * @return ActiveQuery $this
	 */
	public function published()
	{
		$this->andWhere(Post::tableName() . '.status_id = :status', [':status' => Post::STATUS_PUBLISHED]);
		return $this;
	}

	/**
	 * Выбираем только неопубликованые записи.
	 * @param ActiveQuery $query
	 * @return ActiveQuery $this
	 */
	public function unpublished()
	{
		$this->andWhere(Post::tableName() . '.status_id = :status', [':status' => Post::STATUS_UNPUBLISHED]);
		return $this;
	}

	/**
	 * Выбираем только записи которые не прошли модерацию.
	 * @param ActiveQuery $query
	 * @return ActiveQuery $this
	 */
	public function notapproved()
	{
		$this->andWhere(Post::tableName() . '.status_id = :status', [':status' => Post::STATUS_NOT_APPROVED]);
		return $this;
	}
}