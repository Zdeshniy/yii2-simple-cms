<?php
namespace common\modules\blogs\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * Class Post
 * @package common\modules\blogs\models
 * Модель постов.
 *
 * @property integer $id ID
 * @property string $email E-mail
 * @property integer $create_time Время создания
 */
class Subscription extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestampBehavior' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'create_time'
				]
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%subscription}}';
	}

    /**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записи по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			// E-mail [[email]]
			['email', 'required'],
			['email', 'filter', 'filter' => 'trim'],
			['email', 'string', 'max' => 100],
			['email', 'email'],
			['email', 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
		    'id' => 'ID',
		    'email' => 'E-mail',
			'create_time' => 'Дата создания'
		];
	}
}