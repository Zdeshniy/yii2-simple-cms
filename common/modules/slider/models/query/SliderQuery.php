<?php
namespace common\modules\slider\models\query;

use yii\db\ActiveQuery;
use common\modules\slider\models\Slider;

/**
 * Class PostQuery
 * Класс кастомных запросов модели [[Post]]
 */
class SliderQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованые записи.
	 * @param ActiveQuery $query
	 * @return ActiveQuery $this
	 */
	public function published()
	{
		$this->andWhere(Slider::tableName() . '.status_id = :status', [':status' => Slider::STATUS_PUBLISHED]);
		return $this;
	}
}