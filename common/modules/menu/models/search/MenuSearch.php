<?php
namespace common\modules\menu\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\menu\models\Menu;

/**
 * Общая модель поиска по постам блога
 */
class MenuSearch extends Model
{
	public $name;
	public $code;
	public $description;
	public $status_id;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'code', 'description'], 'string'],
			[['status_id'], 'boolean'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'name' => 'Название',
			'code' => 'Уникальный идентификатор',
			'description' => 'Описание',
			'status_id' => 'Статус'
		];
	}

	public function search($params)
	{
		$query = Menu::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'name', true);
		$this->addCondition($query, 'code', true);
		$this->addCondition($query, 'description', true);
		$this->addCondition($query, 'status_id');

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
