<?php
namespace common\modules\materials\models\query;

use yii\db\ActiveQuery;
use common\modules\materials\models\Material;

/**
 * Class PageQuery
 * @package common\modules\blogs\models\query
 * Класс кастомных запросов модели [[Page]]
 */
class MaterialQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованные материалы
	 * @param ActiveQuery $query
	 */
	public function published()
	{
		$this->andWhere('status_id = :status', [':status' => Material::STATUS_PUBLISHED]);
		return $this;
	}

	/**
	 * Выбираем только родительские материалы
	 * @param ActiveQuery $query
	 */
	public function parents()
	{
		$this->andWhere('parent_id = :parent', [':parent' => 0]);
		return $this;
	}
}