<?php
namespace common\modules\seo\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Модуль таблицы {{%seo}}
 *
 * @property integer $model_id ID модели
 * @property string $model_class Класс модели
 * @property string $title СЕО заголовок
 * @property string $meta_description Мета описание
 * @property string $css Список CSS файлов
 * @property string $js СПисок Javascript файлов
 */
class Seo extends ActiveRecord
{
    /**
     * Белый шаблон.
     */
    const COLOR_WHITE = 0;

    /**
     * Черный шаблон.
     */
    const COLOR_BLACK = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seo}}';
    }

    /**
     * @return array Массив доступных цветов шаблона.
     */
    public static function getColorArray()
    {
        return [
            self::COLOR_WHITE => 'Белый',
            self::COLOR_BLACK => 'Черный'
        ];
    }

    /**
     * @return array Массив доступных шаблонов.
     */
    public static function getLayoutArray()
    {
        return [
            '' => 'Выберите шаблон',
            '/home' => 'Шаблон главной',
            '/blogs' => 'Шаблон блога',
            '/contacts' => 'Шаблон обратной связи',
            '/main-fluid' => '1250px',
            '/main-static' => '980px',
            '/main-full' => '100%'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'purifier' => [
                'class' => 'common\behaviors\PurifierBehavior',
                'textAttributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['seo_title', 'meta_description', 'meta_keywords', 'layout', 'page_class', 'js', 'css'],
                    ActiveRecord::EVENT_BEFORE_INSERT => ['seo_title', 'meta_description', 'meta_keywords', 'layout', 'page_class', 'js', 'css']
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['seo_title', 'string', 'max' => 70],
            ['meta_description', 'string', 'max' => 156],
            ['meta_keywords', 'string'],
            [['layout', 'page_class'], 'string', 'max' => 50],
            [['css', 'js'], 'string', 'max' => 255],
            ['color', 'in', 'range' => array_keys(self::getColorArray())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'seo_title' => 'SEO заголовок',
            'meta_description' => 'Мета описание (meta description)',
            'meta_keywords' => 'Мета описание (meta keywords)',
            'layout' => 'Шаблон',
            'color' => 'Цвет',
            'page_class' => 'CSS класс страницы',
            'css' => 'Список CSS файлов',
            'js' => 'Список JS файлов',
        ];
    }
}
